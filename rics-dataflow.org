#+TITLE: RICS Dataflow
#+STARTUP: showall
#+OPTIONS: toc:2 H:4 timestamp:t ^:{}
#+EMAIL: mikek@apl.uw.edu
#+DATE: 2016-02-24
#+LATEX_CLASS: mfk-org-article
#+LATEX_HEADER: \fancyfoot[CE,CO]{RICS Dataflow}
#+SETUPFILE: theme-readtheorg-local.setup


* Introduction
The RICS functionality is provided by multiple co-operating processes (see
Figure [[fig:dataflow]]):

  - Beacon Monitor
  - DIRS Monitor
  - GICS Monitor
  - Data Manager
  - Controller
  - User Interface

#+begin_src dot :exports results :file dataflow.svg :cmdline -Kdot -Tsvg
digraph df {
    subgraph cluster_rics {
        label="RICS";
        labeljust="l";
        labelloc="b";
        bm [label="Beacon\nMonitor" shape=oval];
        dm [label="DIRS\nMonitor" shape=oval];
        gm [label="GICS\nMonitor" shape=oval];
        ctl [label="Controller" shape=oval];
        mgr [label="Data\nManager" shape=oval];
        db [label="Database" shape=box];
        ui [label="User\nInterface" shape=oval];
    }

    rtr [label="RTR*N" shape=box];
    dirs [label="DIRS" shape=box];
    gics [label="GICS" shape=box];
    rads [label="RADS" shape=box];

    rtr -> bm;
    bm -> ctl;
    bm -> mgr;
    ctl -> rtr;
    rtr -> mgr;
    dirs -> dm;
    gics -> gm;
    dm -> mgr;
    gm -> mgr;
    mgr -> db;
    mgr -> rads;
    mgr -> ui;
    ui -> ctl;
    {rank=same; dirs, rtr}
}
#+end_src

#+CAPTION: Dataflow Diagram
#+ATTR_LATEX: :float t :width 10cm :placement [htbp]
#+NAME: fig:dataflow
#+RESULTS:
[[file:dataflow.svg]]

* Components
The RICS processes communicate with each other using [[http://zeromq.org][ZeroMQ]] sockets.

** Beacon Monitor
This process listens for the UDP beacon packets broadcast by the RTR and
writes the packet contents and metadata to a PUB socket
at the endpoint /tcp://*:10131/. In
addition, the GPS and (optional) engineering data are sent via a PUSH
socket to the Data Manager.

***  Message Formats
The GPS and engineering data are sent in the Data Stream packets described
in the /RTR Network Protocols/ document. The input beacon UDP packet is
described in the same document.

**** Output Beacon Message
This message consists of a single JSON object with the following
attributes:

#+ATTR_LATEX: :align |l|l|l|
| *Attribute* | *Type*  | *Description*                         |
|-------------+---------+---------------------------------------|
| rtr         | integer | RTR node ID                           |
| time        | integer | Time in seconds since 1/1/1970 UTC    |
| slat        | integer | Latitude in decimal degrees * $10^6$  |
| slon        | integer | Longitude in decimal degrees * $10^6$ |
| mode        | integer | Fix mode (1, 2, or 3)                 |
| cmdport     | integer | TCP port for command interface        |
| ipaddr      | string  | RTR IP address                        |

** DIRS Monitor
This process monitors an RS232 serial port for timestamp messages from the
DIRS computer and sends the information to the Data Manager via a PUSH
socket.

*** Message Formats
The ASCII input message from DIRS consists of one or more of the records
shown below followed by a "trigger" record which consists of a single
ASCII =X=. Records are terminated by CRLF and all fields within a record
are separated by TABs:

#+begin_example
dd/mm/YYYY HH:MM:SS t1 t2 t3 t4 -1 -1 -1 -1 t1 t2 t3 t4 -1 -1 -1 -1
                    |platform |             |  unit   |
#+end_example

The timestamp values are sent to the Data Manager in the Data Stream format
described in the /RTR Network Protocols/ document. The RTR node ID field
in these packets is set to the DIRS hydrophone number plus 0x80.

** GICS Monitor
This process monitors an RS232 serial port for grid-orientation messages
from the GICS computer and sends the information to the Data Manager via a
PUSH socket.

*** Message Formats
The ASCII input message from DIRS is described below, all fields are space
separated and the message is terminated by CRLF:

#+BEGIN_EXAMPLE
YYYY-mm-dd HH:MM:SS LATITUDE LONGITUDE AZIMUTH
#+END_EXAMPLE

The grid values are sent to the Data Manager in the Data Stream format
described in the /RTR Network Protocols/ document. The RTR node ID field
in these packets is set to zero. The body of the packet is a JSON
encoded object with the following attributes (all of the values are sent as
strings to avoid changing the precision of the float-point numbers):

#+ATTR_LATEX: :align |l|l|l|
| *Attribute* | *Type*  | *Description*                       |
|-------------+---------+-------------------------------------|
| lat         | string  | Origin latitude in decimal degrees  |
| lon         | string  | Origin longitude in decimal degrees |
| az          | string  | Y-axis azimuth in degrees True      |

** Data Manager
This process opens a PULL socket bound to /tcp://*:10130/ and accepts Data
Stream packets containing timestamp, diagnostics, GPS, and engineering
data. The GPS, diagnostics, and engineering data are stored in a database
and are reformatted as JSON messages and written to a PUB socket bound to
the endpoint /tcp://*:10132/. The timestamp values are reformatted and
sent to the RADS computer via RS232.
*** Message Formats
The input Data Stream packets are described in the /RTR Network Protocols/
document.

**** Output to RADS
The ASCII output message to RADS consists of one or more of the records
described below (all having the same date-time) followed by a "trigger"
record. The trigger record is a single ASCII =X=. All fields within a
record are TAB separated and each record (including the trigger) is
terminated by CRLF:

#+begin_example
dd/mm/YYYY HH:MM:SS t1 t2 t3 t4 t1 t2 t3 t4 t1 t2 t3 t4 t1 t2 t3 t4
                    |  DIRS   | |   RTR   | |   DIRS  | |   RTR   |
                    |      platform       | |        unit         |
#+end_example

**** Output GPS Message
The message consists of two ZeroMQ frames. The first frame contains the
ASCII string =GPS= and the second frame contains a single JSON object with
the following attributes:

#+ATTR_LATEX: :align |l|l|l|
| *Attribute* | *Type*  | *Description*                      |
|-------------+---------+------------------------------------|
| rtr         | integer | RTR node ID                        |
| time        | integer | Time in seconds since 1/1/1970 UTC |
| lat         | string  | Latitude in decimal degrees        |
| lon         | string  | Longitude in decimal degrees       |
| mode        | integer | fix mode (1, 2, or 3)              |

**** Output Engineering Message
The message consists of two ZeroMQ frames. The first frame contains the
ASCII string =ENG= and the second frame contains a single JSON object with
the following attributes:

#+ATTR_LATEX: :align |l|l|l|
| *Attribute* | *Type*  | *Description*                      |
|-------------+---------+------------------------------------|
| rtr         | integer | RTR node ID                        |
| time        | integer | Time in seconds since 1/1/1970 UTC |
| vbatt       | string  | Battery voltage in volts           |
| ibatt       | string  | Battery current in amps            |
| temp        | string  | Internal temperature in degC       |

**** Output Diagnostic Message
The message consists of two ZeroMQ frames. The first frame contains the
ASCII string =DIAG= and the second frame contains a single JSON object with
the following attributes:

#+ATTR_LATEX: :align |l|l|l|
| *Attribute* | *Type*  | *Description*                      |
|-------------+---------+------------------------------------|
| rtr         | integer | RTR node ID                        |
| time        | integer | Time in seconds since 1/1/1970 UTC |
| contents    | string  | ASCII data record sent by RTR      |

**** Output Grid Message
     :PROPERTIES:
     :CUSTOM_ID: gridmsg
     :END:
The message consists of two ZeroMQ frames. The first frame contains the
ASCII string =GRID= and the second frame contains a single JSON object with
the following attributes:

#+ATTR_LATEX: :align |l|l|l|
| *Attribute* | *Type*  | *Description*                       |
|-------------+---------+-------------------------------------|
| time        | integer | Time in seconds since 1/1/1970 UTC  |
| lat         | string  | Origin latitude in decimal degrees  |
| lon         | string  | Origin longitude in decimal degrees |
| az          | string  | Y-axis azimuth in degrees True      |

*** Database
The database will be implemented in an [[http://www.sqlite.org][SQLite]] file which will be rotated
daily. There will be a separate table for each message type (/gps/, /eng/,
/diag/, etc.).

** Controller
This process subscribes to messages from the Beacon Monitor and maintains
a list of active RTR nodes. It also binds a ROUTER socket
to the endpoint /tcp://*:10133/ on which it accepts
messages to forward to an RTR. Processes wishing to send messages to the
Controller must connect a DEALER socket to this endpoint.

*** Message Formats

Each message is comprised of four ZeroMQ frames. The first frame is a
message time-stamp in seconds since 1/1/1970 UTC encoded as an ASCII
string.  Normally, the second frame contains the RTR node ID and the
remaining frames are the RTR command message (described in the /RTR
Network Protocols/ document).

#+begin_example
Frame 0: <timestamp> (string)
Frame 1: <RTR ID> (string)
Frame 2: <RTR command> (string)
Frame 3: <arguments> (string)
#+end_example

The Controller responds with a three frame message:

#+begin_example
Frame 0: <RTR ID> (string)
Frame 1: <RTR response> (string)
Frame 2: <arguments> (string)
#+end_example

If the second frame of the outgoing message is empty or set to "0", the
command is interpreted by the Controller. The following commands are
supported:

**** FORGET
This command forces the Controller to "forget" one or more RTR
nodes. Normally, the Controller detects =BYE= commands and automatically
removes the addressed RTR from the active list, this command would only
be used if an RTR goes off-line "abnormally".

#+begin_example
Frame 0: <timestamp> (string)
Frame 1: <empty>
Frame 2: FORGET (string)
Frame 3: <list of space separated RTR IDs> (string)
#+end_example

The Controller responds with a list of the currently active nodes:

#+begin_example
Frame 0: <empty>
Frame 1: NODES (string)
Frame 2: <list of space separated RTR IDs> (string)
#+end_example

**** NODES
Request the list of currently active nodes.

#+begin_example
Frame 0: <timestamp> (string)
Frame 1: <empty>
Frame 2: NODES (string)
Frame 3: <empty>
#+end_example

The response is the same as for the =FORGET= command:

#+begin_example
Frame 0: <empty>
Frame 1: NODES (string)
Frame 2: <list of space separated RTR IDs> (string)
#+end_example

**** AUTOSTART
Set the state of the /autostart/ flag. Normally when an RTR joins the
network, the Controller automatically sends a =HELLO= command to start the
data-stream.

#+begin_example
Frame 0: <timestamp> (string)
Frame 1: <empty>
Frame 2: AUTOSTART (string)
Frame 3: <ON or OFF> (string)
#+end_example

The Controller responds with the flag state:

#+begin_example
Frame 0: <empty>
Frame 1: AUTOSTART (string)
Frame 2: <flag state> (string)
#+end_example

If Frame 3 of the request is left empty, the state will not be
changed. This provides a way to check the current state without altering
it.

** User Interface
This process subscribes to messages from the Data Manager and connects a
DEALER socket to the endpoint provided by the Controller. It provides a
real-time status display and allows the User to control and configure the
RTR nodes.

* Implementation

The RICS computer is a rack-mount PC running the Debian 8 (aka "jessie")
Linux distribution. The RIC processes run under the System Operator
account, =rtr=, and are managed by the
[[http://www.freedesktop.org/wiki/Software/systemd/][systemd Service Manager]]. Under normal circumstances, no user-interaction
is required.

** Input/Output

Communication with the RTR nodes occurs over the Avalan wireless modem
connected to the second Ethernet interface, /eth1/. Communication with
RADS, DIRS, and GICS occurs over the serial ports listed in the table
below:

#+ATTR_LATEX: :align |l|l|
| *Port*       | *Use*                  |
|--------------+------------------------|
| /dev/ttyS1   | Serial output to RADS  |
| /dev/ttyUSB0 | Serial input from DIRS |
| /dev/ttyUSB1 | Serial input from GICS |

Note that =/dev/ttyS1= is the DB-9 on the rear panel of the PC. The other
two serial ports are on the USB-serial hub.

** Software Installation

The RICS application software is distributed by cloning Git
repositories. There are two repository directories under =~rtr/src/=:

- =rics-apps= contains the application programs
- =rics-services= contains the service configuration files.

To download and install the latest revision of the software, run the
following commands in each of the above directories:

#+BEGIN_EXAMPLE
$ git pull
$ make install
#+END_EXAMPLE

** Service Management

The =systemctl= program is used to start, stop, and check the status of
the services. All services are started automatically at boot time.

*** Service Names

- /beaconmon/ Beacon Monitor
- /dirsmon/ DIRS Monitor
- /datamgr/ Data Manager
- /rtrctl/ Controller
- /gicsmon/ GICS Monitor
- /webapp/ User Interface

*** Start a Service
#+BEGIN_EXAMPLE
$ systemctl --user start <service_name>
#+END_EXAMPLE

*** Stop a Service
#+BEGIN_EXAMPLE
$ systemctl --user start <service_name>
#+END_EXAMPLE

*** Check Status
#+BEGIN_EXAMPLE
$ systemctl --user status <service_name>
#+END_EXAMPLE

*** View Logs

This example displays the logs in reverse chronological order (newest
entry first).

#+BEGIN_EXAMPLE
$ journalctl -r --user --user-unit <service_name>
#+END_EXAMPLE

The /-f/ option shows a continuously updating view of the log (ala /tail
-f/). Use =ctrl-c= to terminate.

#+BEGIN_EXAMPLE
$ journalctl -f --user --user-unit <service_name>
#+END_EXAMPLE


** Data Storage

All of the SQLite database files are stored in the =data= subdirectory of
the =rtr= user account. A new file is created at midnight UTC each day.

*** Database Schema

#+INCLUDE: "../apps/db_schema.sql" src sql

** User Interface

The User Interface for monitoring the data-stream from the RTR nodes is
provided by a web-application at http://localhost:5000.

*** RTR Configuration

In addition to the monitoring page, the web-application provides an
end-point which allows the user to make changes to the configuration of
each RTR node. The HTTP /GET/ method is used to read the configuration and
the /PUT/ method is used to update the configuration.

#+BEGIN_EXAMPLE
http://localhost:5000/rtr/cfg/<node-id>[/<key>]
#+END_EXAMPLE

Where /node-id/ is the RTR-ID (1-4) and /key/ is the optional
configuration variable and is only used with the GET method. If no
variable name is given, the state of all configuration variables is
returned.

The input format for /PUT/ is a JSON file containing configuration
variable names (keys) and their new values:

#+BEGIN_EXAMPLE
{ "detect/var_a_limit_default": 0.3, "detect/var_b_limit_default": 10}
#+END_EXAMPLE

If the record above is stored in a file named =test.json=, it can be sent
to RTR-1 using =curl=:

#+BEGIN_EXAMPLE
$ curl -X PUT http://localhost:5000/rtr/cfg/1 -d @test.json
#+END_EXAMPLE

The output format from /GET/ has the following JSON format:

#+BEGIN_EXAMPLE
{
    "reply": "GET",
    "attrs": {
        "filter/channel": 1,
        "coeff_file": "firAB.txt",
          .
          .
          .
        "detect/threshold_a_percent": 20,
        "detect/threshold_a_percent": 20}
}
#+END_EXAMPLE
